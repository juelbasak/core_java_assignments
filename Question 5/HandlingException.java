public class HandlingException 
{
    public static void main(String args[])
    {
        String name = args[0];
        int age = Integer.parseInt(args[1]);

        try
        {
            if(age < 18)
                throw new myException("Age Entered is less!");

            else if (age >= 60)
                throw new myException("Age Entered is more!");
            
            else
                System.out.println("Age entered is accepted!");
        }
        catch(myException exp)
        {
            System.out.println(exp);
        }


    }
}
class myException extends Exception
{
    myException(String exp)
    {
        super(exp);
    }
}