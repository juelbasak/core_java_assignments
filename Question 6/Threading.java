import java.text.SimpleDateFormat;
import java.util.Date;

public class Threading extends Thread
{
    @Override
    public void run() {

    }

    public static void main(String[] args)
    {
        Thread t1 = Thread.currentThread();

        System.out.println("Previous Thread Name : " + t1.getName());

        t1.setName("MyThread");

        System.out.println("Changed Thread Name : " + t1.getName());

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

        System.out.println("Time Captured at " + formatter.format(new Date()));
        System.out.println("Wait for 10 secs....");
        try {
            t1.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("Exception " + e);
        }

        System.out.println("Time Captured after 10 seconds : " + formatter.format(new Date()));

    }
}
